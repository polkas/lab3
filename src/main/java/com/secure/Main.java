package com.secure;

import com.secure.utils.Result;
import com.secure.utils.Helper;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        String secret = "qwertyuiopasdfghjkl";
        String data = "Guys who understand that using Hash function as Mac is one very bad practice: Thai Duong; Juliano Rizzo; Flickr (the hard way);";
        String append = "Yaroslav Oshyiko";

        hack(secret, data, append);

    }

    private static boolean hack(String secret, String data, String append) throws IOException {
        Client client = new Client();
        Server server = new Server();

        String hash = server.getHash(secret, data);
        System.out.println("Server SHA 1 hash for provided secret and data: " + hash);
        int counter = 1;
        boolean flag;
        while(true) {
            Result result = client.getResult(data, append, counter, hash);

            byte[] hmac = result.hmac;
            byte[] message = result.message;


            String serverHmac = client.getServerHmac(secret.getBytes(), message);
            String hexHmac = Helper.toHexString(hmac);

            System.out.println("Trying secret length: " + counter);
            System.out.println("Client hash == Server hash: " + hexHmac + " == " + serverHmac);


            if (serverHmac.equals(hexHmac)) {
                flag = true;
                System.out.println("Hacked!!!!");
                break;
            }

            System.out.println();
            counter++;
        }
        return flag;
    }
}
