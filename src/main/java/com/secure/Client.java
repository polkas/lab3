package com.secure;

import com.secure.utils.SHA1;
import com.secure.utils.Result;
import com.secure.utils.Helper;

public class Client {

    public Result getResult(String message, String append, int secretLength, String oldHash) {

        int bytesAfterData = 64 - ((secretLength + message.length()) % 64);

        if (bytesAfterData <= 8) {
            bytesAfterData += 64;
        }

        int oldLength = (secretLength + message.length()) * 8;

        int paddingSize = bytesAfterData - 8;

        byte[] paddingPart = new byte[paddingSize];

        byte[] sizePart = getSizePart(oldLength);

        //message minus secret
        byte[] resultMessage = createMessage(message.getBytes(), paddingPart, sizePart, append.getBytes());

        byte[] resultHmac = getHmac(oldHash, append, message.getBytes(), paddingPart, sizePart);

        return new Result(resultHmac, resultMessage);
    }

    private byte[] getSizePart(int length) {
        byte[] sizePart = new byte[8];

        int index = sizePart.length - 1;

        while (length > 0) {
            sizePart[index] = (byte) (0xFF & length);
            length = length >> 8;
            index--;
        }

        return sizePart;
    }

    private byte[] createMessage(byte[] old, byte[] padding, byte[] size, byte[] append) {
        byte[] message = new byte[old.length + padding.length + size.length + append.length];

        int position = 0;

        System.arraycopy(old, 0, message, position, old.length);

        position += old.length;

        System.arraycopy(padding, 0, message, position, padding.length);

        position += padding.length;

        System.arraycopy(size, 0, message, position, size.length);

        position += size.length;

        System.arraycopy(append, 0, message, position, append.length);

        message[old.length] = (byte)0x80;

        return message;
    }

    private byte[] getHmac(String hash, String append, byte[] message, byte[] padding, byte[] size) {
        SHA1 sha1 = Helper.initSHA1(hash);
        return sha1.digest(append.getBytes(), message.length + padding.length + size.length);
    }

    public String getServerHmac(byte[] secret, byte[] newMsg) {
        byte[] message = new byte[secret.length + newMsg.length];

        System.arraycopy(secret, 0, message, 0, secret.length);
        System.arraycopy(newMsg, 0, message, secret.length, newMsg.length);

        return Helper.getRealShaString(message);
    }
}
