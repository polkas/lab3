package com.secure.utils;

public class Result {

    public byte[] hmac;

    public byte[] message;

    public Result(byte[] hmac, byte[] message) {
        this.hmac = hmac;
        this.message = message;
    }

}
