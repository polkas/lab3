package com.secure;

import com.secure.utils.SHA1;
import com.secure.utils.Helper;

public class Server {

    public byte[] getHashBytes(String secret, String message) {
        SHA1 sha1 = new SHA1();
        return sha1.digest((secret + message).getBytes());
    }

    public String getHash(String secret, String message) {
        return Helper.toHexString(getHashBytes(secret, message));
    }

}
